#include <Adafruit_VC0706.h>
#include <SPI.h>
#include <SD.h>
#include <SoftwareSerial.h>   
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266mDNS.h> 
#include <FirebaseArduino.h> //servo motor module library

#define chipSelect D10 //pin SDCard


SoftwareSerial cameraconnection = SoftwareSerial(D14, D15); //Define TX and RX
Adafruit_VC0706 cam = Adafruit_VC0706(&cameraconnection); //Define Camera

// SetWiFi SSID and Password
#define wifiSSID "Your SSID"
#define wifiPassword "Your Password SSID"

// Set Firebase Host and Authentication
#define firebaseHost "Your Firebase Host"
#define firebaseAuth "Your Firebase Authentication"

// change to your server
const char* server = "Your Server Name";

WiFiClient client;
WiFiClient dclient;
File webFile;
int status = WL_IDLE_STATUS;     // the Wifi radio's status

char outBuf[128];
char outCount;
char fileName[13] = "Image.JPG";
#define MTU_Size    10*1460  // this size seems to work best

byte clientBuf[MTU_Size];
int clientCount = 0;

String pathCam = "camera/001";

void setupFirebase(){
  //To Connect Firebase
  Firebase.begin(firebaseHost, firebaseAuth);
  Serial.println("Connected Firebase");
}

void setupWiFi(){
  //To Connect the WiFi
  WiFi.begin(wifiSSID, wifiPassword);
  Serial.print("WiFi connecting");
  while (WiFi.status() != WL_CONNECTED){
    Serial.print(".");
    delay(500); 
  }
  Serial.println();
  Serial.println("WiFi connected to:");
  Serial.println(WiFi.SSID());
  Serial.println("IP Address:");
  Serial.println(WiFi.localIP());
}

void TakePict(){
  Serial.println("VC0706 Camera snapshot test");
  
  // Try to locate the camera
  if (cam.begin()) {
    Serial.println("Camera Found:");
  } else {
    Serial.println("No camera found?");
    return;
  }

  // Print out the camera version information (optional)
  char *reply = cam.getVersion();
  if (reply == 0) {
    Serial.print("Failed to get version");
  } else {
    Serial.println("-----------------");
    Serial.print(reply);
    Serial.println("-----------------");
  }

  // Set the picture size 
  //cam.setImageSize(VC0706_640x480);        // biggest
  cam.setImageSize(VC0706_320x240);        // medium
  //cam.setImageSize(VC0706_160x120);          // small

  // Read the size
  uint8_t imgsize = cam.getImageSize();
  Serial.print("Image size: ");
  if (imgsize == VC0706_640x480) Serial.println("640x480");
  if (imgsize == VC0706_320x240) Serial.println("320x240");
  if (imgsize == VC0706_160x120) Serial.println("160x120");
  
  Serial.println("Snap a Picture...");
  delay(3000); //To make the quality better. wait for a sec
  
  if (!cam.takePicture()) 
    Serial.println("Failed to snap!");
  else 
    Serial.println("Picture taken!");
  
  //Delete an image if already exist
  if (SD.exists(fileName)) {
    SD.remove(fileName);
    Serial.println("Delete the file exist");  
  }
  
  // Create an image with the name fileName
  File imgFile = SD.open(fileName, FILE_WRITE);
  
  // Get the size of the image (frame) taken  
  uint16_t jpglen = cam.frameLength();
  Serial.print("Storing ");
  Serial.print(jpglen, DEC);
  Serial.print(" byte image.");

  // pinMode(8, OUTPUT);
  // Read all the data up to # bytes!
  byte wCount = 0; // For counting # of writes
  while (jpglen > 0) {
    // read 32 bytes at a time;
    uint8_t *buffer;
    uint8_t bytesToRead = _min(32, jpglen); // change 32 to 64 for a speedup but may not work with all setups!
    buffer = cam.readPicture(bytesToRead);
    imgFile.write(buffer, bytesToRead);
    if(++wCount >= 64) { // Every 2K, give a little feedback so it doesn't appear locked up
      Serial.print('.');
      wCount = 0;
    }
    //Serial.print("Read ");  Serial.print(bytesToRead, DEC); Serial.println(" bytes");
    jpglen -= bytesToRead;
  }
  imgFile.close();

  Serial.println("done!");
}

byte doFTP(){
  if (client.connect(server,21)) {
    Serial.println(F("Command connected"));
  }
  else {
    Serial.println(F("Command connection failed"));
    return 0;
  }
  // send login name and password
  if(!eRcv()) return 0;
  client.println(F("USER loonier-listing"));
  //if(!eRcv()) return 0;
  client.println(F("PASS richie08"));
  if(!eRcv()) return 0;
  client.println(F("SYST"));
  if(!eRcv()) return 0;
  client.println(F("Type I"));
  //if(!eRcv()) return 0;
  client.println(F("PASV"));
  if(!eRcv()) return 0;
  char *tStr = strtok(outBuf,"(,");
  int array_pasv[6];
  
  for ( int i = 0; i < 6; i++) {
    tStr = strtok(NULL,"(,");
    array_pasv[i] = atoi(tStr);
    if(tStr == NULL)
      Serial.println(F("Bad PASV Answer"));   
  }

  unsigned int hiPort,loPort;
  hiPort = array_pasv[4] << 8;
  loPort = array_pasv[5] & 255;
  Serial.print(F("Data port: "));
  hiPort = hiPort | loPort;
  Serial.println(hiPort);
  if (dclient.connect(server,hiPort)) {
    Serial.println(F("Data connected"));
  }
  else {
    Serial.println(F("Data connection failed"));
    client.stop();
    return 0;
  }
  // setup to send file file fileName
  client.println(F("CWD /public_html"));
  client.print(F("STOR "));
  client.println(fileName);
  webFile = SD.open(fileName,FILE_READ);
  
  while(webFile.available()){
    //Serial.println("1");
    if (webFile.available()>= MTU_Size){
      clientCount = MTU_Size;
      webFile.read(&clientBuf[0],clientCount);
      //Serial.println("2");
    }
    else{
      clientCount = webFile.available();
      webFile.read(&clientBuf[0],clientCount);
      //Serial.println("3");
    }
    
    if(clientCount > 0){
      if(!eRcv()){
        dclient.stop();
        return 0;
      }
      // send file contents
      dclient.write((const uint8_t *)&clientBuf[0],clientCount);
      // Serial.println("4");
      clientCount = 0;
    }
  }
  webFile.close();
  // send file contents
  //dclient.write(clientBuf,clientCount);
  dclient.stop();
  Serial.println(F("Data disconnected"));
  if(!eRcv()) return 0;
  client.println(F("QUIT"));
  if(!eRcv()) return 0;
  client.stop();
  Serial.println(F("Command disconnected"));
  return 1;
}

byte eRcv(){
  byte respCode;
  byte thisByte;
  while(!client.available()) delay(1);
  
  respCode = client.peek();
  outCount = 0;
  
  while(client.available()){ 
    
    thisByte = client.read();   
    Serial.write(thisByte);
    
    if(outCount < 127){
      outBuf[outCount] = thisByte;
      outCount++;     
      outBuf[outCount] = 0;
    }
  }
  if(respCode >= '4'){
    efail();
    return 0; 
  }
  return 1;
}

void efail(){
  byte thisByte = 0;
  client.println(F("QUIT"));
  while(!client.available()) delay(1);
  
  while(client.available()){ 
    thisByte = client.read();   
    Serial.write(thisByte);
  }
  client.stop();
  Serial.println(F("Command disconnected"));
  Serial.println(F("SD closed"));
}

void setup() {
  Serial.begin(115200);
  
  setupWiFi();

  setupFirebase();
  
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    return;
  }
  
  Serial.println("Ready.");
}

void loop() {
  FirebaseObject camObject = Firebase.get(pathCam);
  bool camCondition = camObject.getBool("condition");

  if (camCondition == true){
    TakePict();
    doFTP();
    camCondition = false;
    String path = pathCam + "/condition";
    Firebase.setBool(path, camCondition);
  }
}
