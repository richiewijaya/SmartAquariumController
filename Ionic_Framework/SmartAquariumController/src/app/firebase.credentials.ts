export const FIREBASE_CONFIG = {
    apiKey: "Your_API_Key",
    authDomain: "Your_Auth_Domain",
    databaseURL: "Your_Database_URL",
    projectId: "Your_ProjectID",
    storageBucket: "Your_Storage_Bucket",
    messagingSenderId: "Your_Sender_ID"
};
