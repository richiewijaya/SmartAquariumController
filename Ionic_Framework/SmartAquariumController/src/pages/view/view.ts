import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Observable } from 'rxjs/Observable';
import { CameraServices } from '../../services/camera/camera.services';
import { cameraModel } from '../../models/camera/camera.model';

/**
 * Generated class for the ViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view',
  templateUrl: 'view.html',
})
export class ViewPage {
  camera$: Observable<any>;

  images= 'http://richiewijaya.comxa.com/SMACO.JPG?random+\=' + Math.random();
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams, 
    private cameraS: CameraServices
  ) 
  {
    this.camera$ = this.cameraS
    .getCamera()
    .snapshotChanges()
    .map(
      changes => {
        return changes.map(c => ({
          key: c.payload.key, ...c.payload.val()
        }))
      }
    )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewPage');
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  takeAPict(cam: cameraModel, lastTake: string){
    let now = new Date();
    let hour = now.getHours();
    let min = now.getMinutes();
    let sec = now.getSeconds();
    console.log(now);
    console.log(hour + min + sec);
    lastTake = now.toDateString() + " - " + hour + ":" + min + ":" + sec;
    console.log(lastTake);
    this.cameraS.takeAPict(cam, true, lastTake);
  } 
}
