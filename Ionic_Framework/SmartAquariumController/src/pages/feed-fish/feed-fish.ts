import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Observable } from 'rxjs/Observable';
import { motorModel } from '../../models/motor/motor.model';
import { MotorServices } from '../../services/motor/motor.services';

@IonicPage()
@Component({
  selector: 'page-feed-fish',
  templateUrl: 'feed-fish.html',
})
export class FeedFishPage {
  motor$: Observable<motorModel[]>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private motorS: MotorServices
  ) { 
    this.motor$ = this.motorS
    .getMotor() // Database List
    .snapshotChanges() // Get key and value
    .map(
      changes => {
        return changes.map(c => ({
          key: c.payload.key, ...c.payload.val() //  Retrieve if there some changes.
        }))
      }
    )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedFishPage');
  }

  turnMotor(motor: motorModel){
    this.motorS.turnMotor(motor, true);
  } 

  getTimer(motor: motorModel, feedTime: Date){
    feedTime = new Date(motor.feedTime);
    let hour = feedTime.getUTCHours(); // if getHours() it will get GMT+7
    let min =  feedTime.getUTCMinutes();
    console.log(hour + ":" + min);
    console.log(feedTime);
    let alarm = true;
    this.motorS.setTimer(motor, hour, min, alarm, feedTime);
  }

  offTimer(motor: motorModel, condition: boolean){
    this.motorS.offTimer(motor, condition);
  }

}
