import { Injectable } from "@angular/core";

import { AngularFireDatabase } from "angularfire2/database";
import { lampModel } from "../../models/lamp/lamp.model";

@Injectable()
export class LampServices{
    private lampRef = this.db.list<any>('lamp');

    constructor(private db: AngularFireDatabase){}

    getLamp(){
        return this.lampRef;
    }

    turnLamp(lamp: lampModel, conLamp: boolean){
        return this.lampRef.update(lamp.key, {
            name: "aquarium lamp",
            condition: conLamp
        });
    }

    timerOn(lamp: lampModel, hour: number, min: number, timerOn:boolean, timeLampOn: Date){
        return this.lampRef.update(lamp.key, {
            onHour: hour,
            onMin: min,
            timerCondition: timerOn,
            onTime: timeLampOn
        });
    }

    timeOff(lamp: lampModel, hour: number, min: number, timeLampOff: Date){
        return this.lampRef.update(lamp.key, {
            offHour: hour,
            offMin: min,
            offTime: timeLampOff
        });
    }

    turnOffTimer(lamp: lampModel, condition: boolean){
        return this.lampRef.update(lamp.key, {
            timerCondition: condition
        });
    }
}