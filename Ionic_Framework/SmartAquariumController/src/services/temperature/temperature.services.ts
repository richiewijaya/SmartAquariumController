import { Injectable } from "@angular/core";

import { AngularFireDatabase } from "angularfire2/database";
import { temperatureModel } from "../../models/temperature/temperature.model";

@Injectable()
export class TemperatureServices{
    private temperatureRef = this.db.list<temperatureModel>('temperature');

    constructor(private db: AngularFireDatabase){}

    getTemperature(){
        return this.temperatureRef;
    }
}