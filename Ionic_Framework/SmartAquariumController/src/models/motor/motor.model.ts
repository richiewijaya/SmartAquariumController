export interface motorModel{
    key?: string;
    name: string;
    condition: boolean;
    feedTime: Date;
    timerCondition: boolean;
    hour: number;
    min: number;
}