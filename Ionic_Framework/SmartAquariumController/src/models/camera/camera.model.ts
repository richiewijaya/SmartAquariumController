export interface cameraModel{
    key?: string;
    condition: boolean;
    lastUpdate: string;
}