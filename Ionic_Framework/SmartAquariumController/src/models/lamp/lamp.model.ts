export interface lampModel{
    key?: string;
    name: string;
    condition: boolean;
    offHour: number;
    offMin: number;
    offTime: Date;
    onHour: number;
    onMin: number;
    onTime: Date;
    timerCondition: boolean;
}