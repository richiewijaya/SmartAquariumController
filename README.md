This research called `Smaco - Smart Aquarium Controller` created by `Richie Wijaya` student at President University. This research using `Firebase` as the database (See `http://firebase.google.com/` for details.), `Arduino` as the Hardware, and `Ionic Framework` as the Mobile Apps.

The hardware used in this research:

- 2 of Arduino WeMos D1 R2
- Motor Servo Tower Pro SG90
- 2 Channel Relay Module
- Waterproof Temperature Sensor DSB1820
- Camera Module VC0706
- Male-to-Female Jumper Cables
- Male-to-Male Jumper Cables
- Solderless Breadboard
- Aquarium Size 31 cm x 19 cm x 24 cm
- LED Aquarium Under Water Light 20cm
- Glass Pipe Heater 25 Watt
- Food container

